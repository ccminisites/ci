# CC Minisites CI

This houses the shared CI yml pipeline for all minisites

---

* CI - Build
  * Theme is brought into wp-content/themes/project: https://gitlab.com/ccminisites/theme
  * Plugin is brought into wp-content/plugins/project: https://gitlab.com/ccminisites/project
  * ACF Formbuilder Plugin is brought into wp-content/plugins/acf-formbuilder: https://gitlab.com/ccminisites/acf-formbuilder
  * cd wp-content/themes/project/
  * npm install --silent --progress=false
  * npm run --silent build
  * rm -rf ./node_modules
  * rm -rf ./sass

---

* CI - Deployment
  * https://gitlab.com/ccminisites/ci/-/raw/master/wordpress-ci.yml
  * complete rebuild of the wordpress
    * remove docroot symbolic link
    * create temp docroot folder
    * create index.html in docroot from https://gitlab.com/ccminisites/ci/-/raw/master/maintenance.html
    * delete build directory
    * recreate build directory
    * untar release into build directory
    * create uploads symbolic link from wp-content/ to ../../uploads
    * create wp-config.php from ../wp-config-xxxx.php where xxxx is production or staging
    * preform all the regular wordpress CI core update db, plugins, options, etc...
    * delete temp docroot directory
    * symbolic link build directory to docroot
  * uploads
    * directory in main: uploads (755)
      * symbolic linked into wp_content/ during deployment
      * shared between production and staging
  * wp-config.php
    * previously created in main directory
      * wp-config-production.php
      * wp-config-staging.php
    * symbolic linked during deployment as just wp-config.php

